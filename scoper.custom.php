<?php

function customize_php_scoper_config( array $config ): array {
	$config['patchers'][] = function( string $filePath, string $prefix, string $content ): string {
		if ( strpos( $filePath, 'wpify/core' ) !== false ) {
			$content = str_replace( $prefix . '\\\\array_merge', 'array_merge', $content );
			$content = str_replace( $prefix . '\\\\wpml_object_id_filter', 'wpml_object_id_filter', $content );
			$content = str_replace( $prefix . '\\\\WP_Post', 'WP_Post', $content );
		}

		return $content;
	};

	return $config;
}
