<?php

namespace WpifyWoo\Managers;

use WpifyWoo\Modules\PricesLog\PricesLogRepository;
use WpifyWoo\Plugin;
use WpifyWoo\Repositories\WooOrderRepository;
use WpifyWooDeps\Wpify\Model\Manager;

/**
 * Class RepositoriesManager
 *
 * @package Wpify\Managers
 * @property Plugin $plugin
 */
class RepositoriesManager {
	public function __construct(
		Manager $manager,
		WooOrderRepository $woo_order_repository,
		PricesLogRepository $prices_log_repository
	) {
		$manager->register_repository( $woo_order_repository );
		$manager->register_repository( $prices_log_repository );
	}
}
