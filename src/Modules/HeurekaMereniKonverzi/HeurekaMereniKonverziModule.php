<?php

namespace WpifyWoo\Modules\HeurekaMereniKonverzi;

use WpifyWoo\Plugin;
use WpifyWooDeps\Wpify\WooCore\Abstracts\AbstractModule;
use WpifyWoo\Models\WooOrderModel;
use WpifyWoo\Repositories\WooOrderRepository;
use WpifyWooDeps\Wpify\Model\OrderItem;


/**
 * Class HeurekaOverenoZakaznikyModule
 *
 * @package WpifyWoo\Modules\HeurekaOverenoZakazniky
 */
class HeurekaMereniKonverziModule extends AbstractModule {

	const MODULE_ID = 'heureka_mereni_konverzi';

	public function __construct(
		private WooOrderRepository $woo_order_repository,
	) {
		parent::__construct();
		$this->setup();
	}

	/**
	 * Setup
	 * @return void
	 */
	public function setup() {
		add_action( 'woocommerce_thankyou', array( $this, 'render_tracking_code' ) );
		add_action( 'wp_footer', array( $this, 'render_product_tracking_code' ) );
	}

	/**
	 * Get the module ID
	 * @return string
	 */
	public function id(): string {
		return self::MODULE_ID;
	}

	/**
	 * Set module name
	 * @return string
	 */
	public function name(): string {
		return __( 'Heureka Měření konverzí', 'wpify-woo' );
	}

	/**
	 * Plugin slug
	 *
	 * @return string
	 */

	public function plugin_slug(): string {
		return Plugin::PLUGIN_SLUG;
	}

	/**
	 *  Get the settings
	 * @return array[]
	 */
	public function settings(): array {
		return array(
			array(
				'id'    => 'api_key',
				'type'  => 'text',
				'label' => __( 'Public key for conversions', 'wpify-woo' ),
				'desc'  => __( 'Enter the public key for the conversion measurement code.' ),
			),
			array(
				'id'      => 'country',
				'type'    => 'select',
				'options' => [
					[
						'label' => 'CZ',
						'value' => 'cz',
					],
					[
						'label' => 'SK',
						'value' => 'sk',
					],
				],
				'label'   => __( 'Country', 'wpify-woo' ),
				'desc'    => __( 'Select country for tracking' ),
				'default' => 'cz'
			),
		);
	}

	/**
	 *
	 */
	public function render_tracking_code( $order_id ) {
		$api_key = $this->get_setting( 'api_key' );
		if ( ! $api_key ) {
			return;
		}

		/** @var WooOrderModel $order */
		$order    = $this->woo_order_repository->get( $order_id );
		$products = [];
		foreach ( $order->line_items as $item ) {
			/** @var OrderItem $item */
			$products[] = [
				'add_product',
				(string) $item->product_id,
				$item->name,
				(string) $item->unit_price_tax_included,
				(string) $item->quantity,
			];
		}
		$country = $this->get_setting( 'country' ) ?: 'cz';
		$url     = '//www.heureka.cz/ocm/sdk.js?version=2&page=thank_you';
		if ( 'sk' === $country ) {
			$url = '//www.heureka.sk/ocm/sdk.js?version=2&page=thank_you';
		}
		$url = apply_filters( 'wpify_woo_heureka_mereni_konverzi_url', $url );
		?>
		<!-- Heureka.cz THANK YOU PAGE script -->
		<script>
			(function (t, r, a, c, k, i, n, g) {
				t['ROIDataObject'] = k;
				t[k] = t[k] || function () {
					(t[k].q = t[k].q || []).push(arguments)
				}, t[k].c = i;
				n = r.createElement(a),
					g = r.getElementsByTagName(a)[0];
				n.async = 1;
				n.src = c;
				g.parentNode.insertBefore(n, g)
			})(window, document, 'script', '<?php echo $url;?>', 'heureka', '<?php echo $country ?>');

			heureka('authenticate', '<?php echo esc_attr( $api_key ); ?>');

			heureka('set_order_id', '<?php echo esc_attr( $order->id ); ?>');
			<?php foreach ( $products as $item ) { ?>
			heureka(<?php echo json_encode( $item );?>);
			<?php }?>
			heureka('set_total_vat', '<?php echo esc_attr( $order->get_wc_order()->get_total() ); ?>');
			heureka('set_currency', '<?php echo esc_attr( $order->get_wc_order()->get_currency() ); ?>');
			heureka('send', 'Order');
		</script>
		<!-- End Heureka.cz THANK YOU PAGE script -->
		<?php
	}

	/**
	 *
	 */
	public function render_product_tracking_code() {
		$api_key = $this->get_setting( 'api_key' );
		if ( ! $api_key || ! is_product() ) {
			return;
		}
		$country = $this->get_setting( 'country' ) ?: 'cz';
		$url     = '//www.heureka.cz/ocm/sdk.js?version=2&page=product_detail';
		if ( 'sk' === $country ) {
			$url = '//www.heureka.sk/ocm/sdk.js?version=2&page=product_detail';
		}
		$url = apply_filters( 'wpify_woo_heureka_mereni_konverzi_url', $url );
		?>
		<!-- Heureka.cz PRODUCT DETAIL script -->
		<script>
			(function (t, r, a, c, k, i, n, g) {
				t['ROIDataObject'] = k;
				t[k] = t[k] || function () {
					(t[k].q = t[k].q || []).push(arguments)
				}, t[k].c = i;
				n = r.createElement(a),
					g = r.getElementsByTagName(a)[0];
				n.async = 1;
				n.src = c;
				g.parentNode.insertBefore(n, g)
			})(window, document, 'script', '<?php echo $url;?>', 'heureka', '<?php echo $country ?>');
		</script>
		<!-- End Heureka.cz PRODUCT DETAIL script -->
		<?php
	}
}
