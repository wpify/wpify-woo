const config = require('@wordpress/scripts/config/webpack.config');

const entry = {
	'settings': [
		'./assets/admin/settings.js',
		'./assets/admin/settings.scss',
	],
	'icdic': [
		'./src/Modules/IcDic/assets/icdic.js',
		'./src/Modules/IcDic/assets/icdic.scss',
	],
	'icdic-blocks': [
		'./src/Modules/IcDic/assets/icdic-blocks.js'
	],
	'delivery-dates': [
		'./src/Modules/DeliveryDates/assets/delivery-dates.js',
		'./src/Modules/DeliveryDates/assets/delivery-dates.scss',
	],
	'prices': [
		'./src/Modules/Prices/assets/prices.scss',
	]
};

module.exports = { ...config, entry };
